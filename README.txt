Custom Input
============

The Custom Input module adds options to the standard Field API Manage Fields UI
to give site builders more control over form display.

In particular, for each supported field type, site builders can specify:
- a user prompt to use on forms in lieu of the field label
- additional CSS classes to apply to the wrapper div, the grouping div (for radios, checkboxes and the like), the input, the label and the help description
- arbitrary HTML attributes to be added to the input, label and help elements

There are also some oddball changes:
You can move the display of the required marker that Drupal adds so that it appears before the label text, instead of after.
Also, the space between the required marker and the label is removed.  Style the span tag to re-introduce it, if you like.
You can move the display of the help text so that it is output after the label, instead of at the end of the field.

Because it irked me to have to install additional modules, and because the limitations of the Prepopulate
and Conditional Field modules were annoying me, it also replicates their functionality to some extent:
- Individual field values may be prepopulated by a URL query parameter.
- You can manipulate the #states attribute of the field.
These additional functions should be used with care.  The prepopulate functionality should be used with extreme care, as it may introduce security issues.

Use cases include:

- adding grid classes to form elements to help with sizing and responsiveness (the original use case)
- adding classes that identify form elements for javascript validation libraries
- using all of the new HTML5 input types like "email", "tel", "url", etc.
- adding attributes that improve the usability and accessibility of the form, such as "placeholder", "autofocus"
- adding attributes that improve the security of form elements, like autocomplete = "off"
- adding attributes to invoke javascript functionality, like Bootstrap's data api.

Custom Input can also be invoked programmatically in custom forms and in form alter functions, so that you don't have to write your
own theming functions.  Note that the prepopulation functionality does NOT work in this context.  Also note that not all forms go through
the standard rendering pipeline.  For instance, exposed forms on views only use the standard rendering pipeline at the control level,
with the result that wrapper-level settings simply don't work (and may have unexpected results).

To use custom_input this way, first add 'custom_input_pre_render' at the end of the #pre_render array of the relevant field:

    if ( !isset($form[FIELD_NAME]['#pre_render']) ) $form[FIELD_NAME]['#pre_render'] = array(); //make sure there is a pre_render array to add to
    $form[FIELD_NAME]['#pre_render'][] = 'custom_input_pre_render';

Then add the custom input settings you want to invoke.  You'll have to look at the settings form in custom_input.admin.inc for the complete list,
but here's an example for a text field that is supposed to accept positive integers:

    $form[FIELD_NAME]['#custom_input'] = array(
        'move_required' => true, // put the required marker before the label
        'advanced' => array(
            'input_attributes' => "type|number\nmin|1", // change the input type to "number", with a "min" value of 1
        ),
    );


Field widgets supported:
Not all field types or widgets are supported, and many are only partially supported.
Adding a class to the wrapper div and changing the prompt will work for the vast
majority of elements.  For partial details, see below.

Tested with:
Text fields/widget -- works
Multiple text fields/widget -- works
Text area/multi row widget - works
Checkboxes -- works.  Note that the "input" class is applied to
the div that wraps around the box and label, not to the checkbox itself.
Select -- works.
Email/text field - works
Autocomplete - works
Link/link - Works for: prompt, wrapper class, help; input class/attr, label class/attr are applied to subfields.
            Does not: group class.
Date/pop-up calendar - Works for: prompt, wrapper class, input and label classes are applied to subfields.
                       Does not: group class, help.
Numeric fields using text widget - should all work, some not tested
Integer/Select or Other checkbox (1 value, not required) - Works for: prompt, wrapper class, help is moved ABOVE the prompt.
                                                           Input class/attr is applied to the "other" box.
Body - Works for: prompt, wrapper class.
Long formatted text - like body.

Does not work with node title fields, which are not conventional fields.

Known Problems
=======================
Zurb Foundation theme tooltips -- if turned on, your help text gets nuked.

Areas for Future Development
========================
Testing, testing, testing.
Add submodules that can modify more complex fields like date popups and addressfields and/or provide hooks for other modules
to supplement what this module does.
Add ability to modify title fields, possibly using the technique employed by the maxlength module.

Technical Details
==================
In overview, the module:

1. Adds a Custom Input fieldset to the field settings form to collect settings info.

2. Creates 4 theme hooks to permit the module to override the default theme hooks
without mucking up non-custom fields.  Respectively: custom_input_element (replaces theme_form_element),
custom_input_element_label (theme_form_element_label), custom_input_multiple_element
(the strangely named theme_field_multiple_value_form), and custom_input_textfield (theme_textfield).
The main purppose of the theme_textfield override is to permit use of HTML5 input types without
having to get the elements module involved.

3. Using hook_field_attach_form(), the module decides whether the field should be
processed by custom_input.  If so, it attaches a copy of the settings to the element
and adds a pre_render function - custom_input_pre_render()

4. The pre_render function applies applicable settings to the current element--
changes overridden theme hooks, applicable prompt, classes and attributes--then
adds itself as a callback to the element's children so it can do it all over again.

Things the module definitely cannot and will never do:
Change the prompt on multi-valued elements.  Although it could add the prompt to the individual values,
this seems unwise.
