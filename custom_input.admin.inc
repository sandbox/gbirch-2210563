<?php

/**
 * @file
 * Admin functions for custom input
 *
 * @author  Tech-Tamer <tech-tamer@tech-tamer.com>
 * @version 0.2
 */

function custom_input_field_settings(&$form, &$form_state, $widget) {
  $settings =& $form['instance']['widget']['settings']; // reference to the current form's settings
  $hsettings =& $form['#instance']['widget']['settings']; // reference to saved settings for this field

  //@TODOGAB -- pare down the options if they don't apply to the current widget
  $multi_field = in_array($widget, array('field_collection_embed'));

  $settings['custom_input'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => empty($hsettings['custom_input']['enabled']),
    '#title' => t('Custom Input Settings'),
    '#description' => '<p>' . t('Not all of these settings will work with all types of fields and widgets. The "Prompt" and "Wrapper class(es)" settings are the most likely to work.  See the README for more details.'),
  );
  if ( $multi_field ) {
    $settings['custom_input']['#description'] .= ' ' . t('For this field type, all input and label settings have been disabled.');
  }
  $settings['custom_input']['#description'] .= '</p>';
  $settings['custom_input']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Custom Input'),
    '#description' => t('Customize the form markup for this field.'),
    '#default_value' => isset($hsettings['custom_input']['enabled']) ? $hsettings['custom_input']['enabled'] : NULL,
  );
  $settings['custom_input']['prompt'] = array(
    '#type' => 'textfield',
    '#title' => t('Prompt'),
    '#description' => t('A custom prompt to use when the field is shown in a form.  Replaces the field label.  May include some HTML.'),
    '#default_value' => isset($hsettings['custom_input']['prompt']) ? $hsettings['custom_input']['prompt'] : NULL,
  );
  $settings['custom_input']['wrapper_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Wrapper class(es)'),
    '#description' => t('Additional class(es) to add to the wrapper element for this field.  Separate multiple classes with spaces.'),
    '#default_value' => isset($hsettings['custom_input']['wrapper_classes']) ? $hsettings['custom_input']['wrapper_classes'] : NULL,
  );
  $settings['custom_input']['group_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Group class(es)'),
    '#description' => t('Additional class(es) to add to the grouping element.  Only applies to fields that hold more than one input: e.g. checkboxes, radios, some date fields, and link fields.'),
    '#default_value' => isset($hsettings['custom_input']['group_classes']) ? $hsettings['custom_input']['group_classes'] : NULL,
  );
  if ( !$multi_field ) {
    $settings['custom_input']['input_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Input class(es)'),
      '#description' => t('Additional class(es) to add to the input element(s).'),
      '#default_value' => isset($hsettings['custom_input']['input_classes']) ? $hsettings['custom_input']['input_classes'] : NULL,
    );
    $settings['custom_input']['label_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Label class(es)'),
      '#description' => t('Additional class(es) to add to the label element.'),
      '#default_value' => isset($hsettings['custom_input']['label_classes']) ? $hsettings['custom_input']['label_classes'] : NULL,
    );
  }
  $settings['custom_input']['move_required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Place required marker before label text'),
    '#description' => t('If checked, the required mark that Drupal adds to required fields will display before the text of the label, instead of the default position after the label text.'),
    '#default_value' => isset($hsettings['custom_input']['move_required']) ? $hsettings['custom_input']['move_required'] : NULL,
  );
  $settings['custom_input']['help_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Help class(es)'),
    '#description' => t('Additional class(es) to add to the help element.'),
    '#default_value' => isset($hsettings['custom_input']['help_classes']) ? $hsettings['custom_input']['help_classes'] : NULL,
  );
  $settings['custom_input']['move_help'] = array(
    '#type' => 'checkbox',
    '#title' => t('Place help after label'),
    '#description' => t('If checked, any help text you have added above will be shown immediately after the label/prompt for this field, instead of the default position at the end of the field markup. Can produce unexpected results for complex widgets.'),
    '#default_value' => isset($hsettings['custom_input']['move_help']) ? $hsettings['custom_input']['move_help'] : NULL,
//      '#states' => array(
//        ':textarea[name="instance[description]"]' => array('filled' => TRUE),
//      ),
  );
  if ( 'options_onoff' == $widget ) {
    $settings['custom_input']['reorder_checkbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Reverse order of checkbox and label'),
      '#description' => t('If checked, the label/prompt for this checkbox field will appear in the reverse of the default position (before if default is after, and vice versa).'),
      '#default_value' => isset($hsettings['custom_input']['reorder_checkbox']) ? $hsettings['custom_input']['reorder_checkbox'] : NULL,
    );
  }
  // advanced settings
  $adv_collapsed = (empty($hsettings['custom_input']['advanced']['input_attr'])
    && empty($hsettings['custom_input']['advanced']['label_attr'])
    && empty($hsettings['custom_input']['advanced']['help_attr'])
    && empty($hsettings['custom_input']['advanced']['prepop'])
  );
  $settings['custom_input']['advanced'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => $adv_collapsed,
    '#title' => t('Custom HTML Attributes and Other Advanced Settings'),
    '#description' => '<p>' . t('Attributes set here will REPLACE existing attributes of the same name.  Do not use these settings unless you know what you are doing.') . '<ul>'
      . '<li>' . t('When entering attributes, separate the name of the attribute from its value with a pipe symbol, like so: placeholder|This is the placeholder value for my text field.') . '</li>'
      . '<li>' . t('Enter one attribute per line. Don\'t use quotation marks.') . '</li>'
      . '<li>' . t('Due to inherent limitations in the Drupal form theming layer, you cannot have naked attributes.  All attributes will be rendered as attr="value".') . '</li>'
      . '</ul></p>',
  );

  if ( !$multi_field ) {
    $settings['custom_input']['advanced']['input_attr'] = array(
      '#type' => 'textarea',
      '#title' => t('Input attributes'),
      '#rows' => 2,
      '#description' => t('HTML attributes to apply to the input element(s) for this field such as a placeholder or an HTML5 input type.'),
      '#default_value' => isset($hsettings['custom_input']['advanced']['input_attr']) ? $hsettings['custom_input']['advanced']['input_attr'] : NULL,
    );
    $settings['custom_input']['advanced']['label_attr'] = array(
      '#type' => 'textarea',
      '#title' => t('Label attributes'),
      '#rows' => 1,
      '#description' => t('HTML attributes to apply to the label element for this field.  Since Drupal already supplies the "for" attribute, this is not commonly needed, and is included only for completeness.'),
      '#default_value' => isset($hsettings['custom_input']['advanced']['label_attr']) ? $hsettings['custom_input']['advanced']['label_attr'] : NULL,
    );
  }
  $settings['custom_input']['advanced']['help_attr'] = array(
    '#type' => 'textarea',
    '#title' => t('Help attributes'),
    '#rows' => 1,
    '#description' => t('HTML attributes to apply to the help element for this field.  This is mainly useful for accessibility, e.g. id|aria-describer.'),
    '#default_value' => isset($hsettings['custom_input']['advanced']['help_attr']) ? $hsettings['custom_input']['advanced']['help_attr'] : NULL,
  );
  if ( !$multi_field ) {
    $settings['custom_input']['advanced']['prepop'] = array(
      '#type' => 'checkbox',
      '#title' => t('Permit pre-population'),
      '#description' => t('Permit this field to be pre-populated from the URL using a query string like this: @example=value', array('@example' => $form['instance']['field_name']['#value']))
        . t('<br />Notes: 1) Be sure to urlencode the value. 2) The value will be run through check_plain() before it ends up on the form.'),
      '#default_value' => isset($hsettings['custom_input']['advanced']['prepop']) ? $hsettings['custom_input']['advanced']['prepop'] : NULL,
    );
    $settings['custom_input']['advanced']['disable_prepop'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable the field if pre-populated'),
      '#description' => t('Disable this field if it has been pre-populated from the URL'),
      '#default_value' => isset($hsettings['custom_input']['advanced']['disable_prepop']) ? $hsettings['custom_input']['advanced']['disable_prepop'] : NULL,
      '#states' => array(
        'visible' => array(
          ':input[name="instance[widget][settings][custom_input][advanced][prepop]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }
  // states
  $states_collapsed = empty($hsettings['custom_input']['states']['state']);
  $settings['custom_input']['states'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => $states_collapsed,
    '#title' => t('Conditional Field'),
    '#description' => '<p>' . t('Drupal has some cool functionality to change the state of a field based on what the user does in another field.  This lets you use some of that functionality.  But honestly, really do not use these settings unless you know what you are doing.') . '<ul>'
      . '<li>' . t('Be sure to read the drupal_process_states() documentation at !url.', array('!url' => l('Drupal.org', 'https://api.drupal.org/api/drupal/includes!common.inc/function/drupal_process_states/7'))) . '</li>'
      . '<li>' . t('This only allows you to add one "state" to the field, which in turn can depend on only one other field.') . '</li>'
      . '<li>' . t('You must identify the state you want the element to have when the condition is met, the other field on which the state of this field depends, and one or more conditions that must be true for that other field.') . '</li>'
      . '</ul></p>',
  );
  $poss_states = array(
    'enabled',
    'disabled',
    'required',
    'optional',
    'visible',
    'invisible',
    'checked',
    'unchecked',
    'expanded',
    'collapsed'
  );
  $settings['custom_input']['states']['state'] = array(
    '#type' => 'select',
    '#options' => array_combine($poss_states, $poss_states),
    '#empty_option' => '- None -',
    '#required' => FALSE,
    '#title' => 'What should happen to this field when the condition is met?',
    '#multiple' => FALSE,
    '#default_value' => isset($hsettings['custom_input']['states']['state']) ? $hsettings['custom_input']['states']['state'] : NULL,
  );
  $settings['custom_input']['states']['remote_field'] = array(
    '#type' => 'textfield',
    '#title' => 'What other field does the state of this field depend on?',
    '#description' => t('Use a jQuery-style string to identify the other field, e.g. :input[name="field_example[und][other]"]'),
    '#default_value' => isset($hsettings['custom_input']['states']['remote_field']) ? $hsettings['custom_input']['states']['remote_field'] : NULL,
  );
  $settings['custom_input']['states']['conditions'] = array(
    '#type' => 'textarea',
    '#title' => t('Conditions'),
    '#rows' => 2,
    '#description' => t('<p>Each line should be a type of condition, a pipe symbol, and the value to be tested. Examples:')
      . t('<ul><li>checked|true</li><li>empty|false</li><li>value|other</li></ul>')
      . t('For this purpose, the words "true" and "false" will be treated as the boolean values they represent.</p>'),
    '#default_value' => isset($hsettings['custom_input']['states']['conditions']) ? $hsettings['custom_input']['states']['conditions'] : NULL,
  );
}